from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Predicting the final Price of a house based on its features using advanced regression techniques',
    author='Caleb Ejakait',
    license='MIT',
)
